---
layout: markdown_page
title: "Category Strategy - Disaster recovery"
---

- TOC
{:toc}

## 🌏 Disaster recovery

| | |
| --- | --- |
| Stage | [Enablement](/direction/Enablement) |
| Maturity | [minimal](/direction/maturity/) |


### Introduction and how you can help

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

* [Overall Strategy](/direction/geo)
* Roadmap - Currently not separate from Geo replication (new category)
* [Maturity: Minimal](/product/categories/maturity)
* [Documentation](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/)
* [All Epics]()
 
GitLab installations hold business critical information and data. The Disaster Recovery(DR) category helps our customers fullfill their business continuity plans by creating processes that allow the recovery of GitLab following a natural or human-created disastery. Disaster recovery complements GitLab's [High Availability configuration](https://about.gitlab.com/solutions/high-availability/) and utilizes [Geo nodes](https://docs.gitlab.com/ee/administration/geo/replication/) to enable a failover in a disaster situation. We want disaster recovery to be robust and easy to use for [systems adminstrators](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator) - especially in a potentially stressful recovery situation.  

⚠️ Currently, there are [some limitations](https://docs.gitlab.com/ee/administration/geo/replication/index.html#current-limitations) of what data is replicated. Please make sure to check the documentation! 

Please reach out to Fabian Zimmer, Product Manager for the Geo group ([Email](mailto:fzimmer@gitlab.com)) if you'd like to provide feedback or ask
any questions related to this product category.

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

## 🔭 Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

Setting up a disaster recovery solution for GitLab requires significant investment and is cumbersome in more complex setups, such as high availability configurations. Geo doesn't replicate all parts of GitLab yet, which means that users need to be aware of what they can recover in case of disaster. 

In the future, our users should be able to use a GitLab Disaster Recovery solution that fits within their business continuity plan. Users should be able to choose which Recovery Time Objective (RTO) and Recovery Point Objective (RPO) are acceptable to them and GitLab's DR solutions should provide configurations that fit those requirements. 

A systems administrator should be able to confidently setup a DR solution even when the setup is complex, as is the case for high availability. In case of an actual disaster, a systems administrator should be able to follow a simple and clear set of instructions that allows them to recover a working GitLab installation. In order to ensure that DR works, frequent failovers should be tested.

* Disaster Recovery should cover different scenarios based on acceptable Recovery Time Objective (RTO) and Recovery Point Objective (RPO). There is always a trade off between the complexity of the system needed given the requirements in a disaster recovery. GitLab's DR strategies should make this explicit to users.
* Disaster Recovery should clearly define which data is replicated and why it is relevant for customers.
* Disaster Recovery should by default allow the recovery of *all* customer relevant data that was available on the production instance. Users should not need to think about caveats or exclusions.
* Disaster Recovery procedures in case of an actual disaster should be as simple as possible. All instructions should fit on on one laptop screen (< 10 steps) that are linear and easy to follow.
* Setting up DR solution(s) should be simple and clearly explain the trade offs for users.
* Disaster Recovery should allow for frequent failover testing that ensure DR is fully functional.
* Disaster recovery should integrate into a more holistic approach that includes High Availability and Geo-distributed configurations.
* Disaster recovery should be complemented by monitoring that can detect a potential disaster.
* The Disaster Recovery solution is actively used on GitLab.com to ensure that all best practices are followed and to ensure that we dogfood our own solutions.
* Disaster recovery solutions should scale from small installations with hundreds of users to extremely large installations with millions of users.

## 🎭 Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

### Sidney (Systems Administrator) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)

* 🙂 **Minimal** - Sidney can manually configure a disaster recovery solution using Geo nodes. More complex configurations, such as HA, are supported but are highly manual to set up. Some data may not be replicated. Failovers are manual.
* 😊 **Viable** - Sidney can configure a disaster recovery setup and all data is replicated. HA configurations are fully supported and all data is replicated.
* 😁 **Complete** - Sidney can choose between different configurations that clearly link back to suggested RTO and RPO requirements. Configuration is simple and all solutions are constantly monitored. A dashboard informs users of the current status. A recovery process is less than <10 steps.
* 😍 **Lovable** - Automatic failovers are supported.

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's Next & Why 
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

* Use Geo to support the Disaster Recovery strategy on GitLab.com - we need to use our own solutions to ensure we are confident in them working properly
* Simplify the disaster recovery process - the current process is highly manual and long - we need to make it as simple as possible for systems administrators to recover in an actual disaster.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

The GitLab DR category is not a replacement for off-site backups and we currently do not plan to include any additional backup methods into our disaster recovery category.

### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). -->

This category is currently at the minimal maturity level, and our next maturity target is viable (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/)).

In order to move this category from  `minimal` to `viable`, one of the main initiatives is to create a simplified disaster recovery process. High Availability configurations are also fully supported and *all data* is replicated.


## 🏅 Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

We have to understand the current DR landscape better and we are actively engaging in customer meetings to understand what features are required to move DR forward.

## Analyst landscape
<!--  What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

We do need to interact more closely with  analysts to understand the landscape better.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

* https://gitlab.com/groups/gitlab-org/-/epics/893

## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

* [Category issues listed by popularity](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Geo&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

## 🦊 Top internal customer issues/epics  
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

* [Geo for DR on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/12)

## Top Strategy Item(s)
<!--  What's the most important thing to move your vision forward?-->

- Replicate *all data*
- Simplify DR process
