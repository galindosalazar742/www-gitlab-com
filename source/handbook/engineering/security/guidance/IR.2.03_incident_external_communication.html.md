---
layout: markdown_page
title: "IR.2.03 - Incident External Communication Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.2.03 - Incident External Communication

## Control Statement

GitLab communicates a response to external stakeholders as required by the Incident Response Plan.

## Context

This control demonstrates that we can provide evidence of communication in the event of an incident to external stakeholders.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.03_incident_external_communication.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.03_incident_external_communication.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.03_incident_external_communication.md).

## Framework Mapping

* PCI
  * 12.10.1
